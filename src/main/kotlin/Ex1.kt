import kotlinx.coroutines.*

fun main() {
    println("The main program is started")
    runBlocking {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
//Ex 1.2
fun doBackground(){
        GlobalScope.launch {
            println("Background processing started")
            delay(3000)
            println("Background processing finished")
        }

}