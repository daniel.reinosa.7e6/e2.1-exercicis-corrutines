import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.system.measureTimeMillis

fun main(){
    val file = File("src/main/kotlin/Cancion").readLines()
    runBlocking {
        launch {
            val time = measureTimeMillis {
                for (i in file){
                    println(i)
                    delay(3000)
                }
            }
            println("${time/1000} s")
        }
    }


}