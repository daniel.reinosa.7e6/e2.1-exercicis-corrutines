import kotlinx.coroutines.*

fun main(){
    runBlocking {
        launch {
            msg1()
        }
    }
    println("Finished!")
}
suspend fun msg1(){
            var count = 0
            for (i in 0 until 10){
                count++
                println("Hello World!")
            }
            delay(100)
}