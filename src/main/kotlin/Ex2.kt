import kotlinx.coroutines.*

fun main(){
    runBlocking {
        msg()
    }
    println("Finished!")
}
suspend fun msg(){
    coroutineScope {
        launch {
            var count = 0
            for (i in 0 until 10){
                count++
                println("$count Hello World!")
            }
            delay(100)
        }
    }
}
