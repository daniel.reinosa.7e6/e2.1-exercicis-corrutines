import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.random.Random

fun main() {
    val sc = Scanner(System.`in`)
    val scan = sc.nextInt()
    val random = (1..50).random()

    println("You have 10 seconds to guess the secret number...")
    println("Enter a number:")
    scan
    if (scan != random) {
        println("Wrong number!")
    }else println("You got it")
    runBlocking {
        launch {
            delay(10000)
        }
            println("The time is up!")
    }




}
