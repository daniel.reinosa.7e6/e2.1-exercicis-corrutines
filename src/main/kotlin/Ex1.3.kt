import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

fun main(){
    println("The main program is started")
    runBlocking {
        doBackground2()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
suspend fun doBackground2(){
    withContext(Dispatchers.Default){
        println("Background processing started")
        delay(3000)
        println("Background processing finished")
    }

}